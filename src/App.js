import React from 'react';
import './App.scss';
import Modalwindow from './Components/Modalwindow/Modalwindow';
import Button from './Components/Button/Button';




class App extends React.Component {
  state = {
    firstIsOpenModal: false,
    secondIsOpenModal: false,
  };
  firstModOpen = () => {
    this.setState({ firstIsOpenModal: true });
    this.setState({ secondIsOpenModal: false });
  }
  secondModOpen = () => {
    this.setState({ secondIsOpenModal: true });
    this.setState({ firstIsOpenModal: false });
  }
firstModClose = () => { this.setState({ firstIsOpenModal: false }); }
secondModClose = () => { this.setState({ secondIsOpenModal: false }); }
  render() {
    const { firstIsOpenModal, secondIsOpenModal } = this.state;

    return (
      <div className="App">
        <Button
          text={"Open first modal"}
          backgroundColor={"red"}
          handleClick={this.firstModOpen}
        />
        <Button
          text={"Открыть второе модальное окно"}
          backgroundColor={"blue"}
          handleClick={this.secondModOpen}
        />

        {firstIsOpenModal && (
          <Modalwindow
            backgroundColor={"red"}
            headertext={"Do you want to delete this file?"}
            modaltext={
              "Once you delete this file, it won't be possible to undo this actions. Are you sure you want to delete it? "
            }
            actions={
              <>
                <Button
                  text={"Ok"}
                  backgroundColor={"rgb(128, 128, 128, 0.5)"}
                  handleClick={this.firstModClose}
                />
                <Button
                  text={"Close"}
                  backgroundColor={"rgb(128, 128, 128, 0.5)"}
                  handleClick={this.firstModClose}
                />
              </>
            }
            isCrossButton={true}
            handleClick={this.firstModClose}
          />
          
       )}
        {secondIsOpenModal && (
          <Modalwindow
            backgroundColor={"blue"}
            headertext={"Хотите удалить этот файл?"}
            modaltext={
              "Как только вы удалите его, это не возможно будет отменить. Уверены, что хотите удалить файл?"
            }
            actions={
              <>
                <Button
                  text={"Удаляем"}
                  backgroundColor={"rgb(128, 128, 128, 0.5)"}
                  handleClick={this.secondModClose}
                />
                <Button
                  text={"Закрыть"}
                  backgroundColor={"rgb(128, 128, 128, 0.5)"}
                  handleClick={this.secondModClose}
                />
              </>
            }
            isCrossButton={true}
            handleClick={this.secondModClose}
          />
        )}
      </div>
    );
  }
}



export default App;

