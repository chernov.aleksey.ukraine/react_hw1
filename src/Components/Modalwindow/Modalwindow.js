import React from "react";
import './Modalwindow.scss'

class Modalwindow extends React.PureComponent {
    
    render() {
        const { headertext, modaltext, actions, isCrossButton, handleClick, backgroundColor } = this.props;
      return (
        <>
          <div className="globalback" onClick={handleClick}></div>
          <div className="modal" style={{ backgroundColor: backgroundColor }}>
            <header className="modalheader">
              <p>{headertext}</p>
              {isCrossButton && (
                <div className="crossbutton" onClick={handleClick}>
                  &#10006;
                </div>
              )}
            </header>
            <main className="modalmain">
              <div>{modaltext}</div>
            </main>
            <div className="modalbuttoncontainer">{actions}</div>
          </div>
        </>
      );
    }
}
export default Modalwindow;