import React from "react";
import  './Button.scss'
class Button extends React.PureComponent {
  render() {
    const { backgroundColor, text, handleClick } = this.props;
    return (
        <div className="generalbutton">
            <button style={{backgroundColor: backgroundColor}} onClick={handleClick}>{text}</button>
        
      </div>
    );
  }
}
export default Button;